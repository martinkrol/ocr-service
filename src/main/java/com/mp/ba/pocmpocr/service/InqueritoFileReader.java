package com.mp.ba.pocmpocr.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Service
public class InqueritoFileReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(FineReaderService.class);

    @Autowired
    FineReaderService readerService;

    public void readAllFiles(String path) {
        try (Stream<Path> paths = Files.walk(Paths.get(path))) {
            paths
                    .filter(p -> p.toString().endsWith(".pdf"))
                    .filter(Files::isRegularFile)
                    .forEach(p -> {
                        try{
                        readerService.processaImagem(path, p.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }); //TODO: Chamar função do FileReaderService
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
