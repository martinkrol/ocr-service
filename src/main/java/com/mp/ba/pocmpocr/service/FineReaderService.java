package com.mp.ba.pocmpocr.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.*;

import com.google.gson.Gson;
import com.mp.ba.pocmpocr.config.CharacterData;
import com.mp.ba.pocmpocr.config.Configuration;
import com.mp.ba.pocmpocr.config.FileImageSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abbyy.FREngine.Engine;
import com.abbyy.FREngine.FileExportFormatEnum;
import com.abbyy.FREngine.IBatchProcessor;
import com.abbyy.FREngine.IEngine;
import com.abbyy.FREngine.IFRPage;
import com.abbyy.FREngine.ILicenses;
import com.abbyy.FREngine.IObjectsExtractionParams;
import com.abbyy.FREngine.IOrientationDetectionParams;
import com.abbyy.FREngine.IPageAnalysisParams;
import com.abbyy.FREngine.IPagePreprocessingParams;
import com.abbyy.FREngine.IPageProcessingParams;
import com.abbyy.FREngine.IPrepareImageMode;
import com.abbyy.FREngine.IRecognizerParams;
import com.abbyy.FREngine.ITextExportParams;
import com.abbyy.FREngine.MessagesLanguageEnum;
import com.abbyy.FREngine.OrientationDetectionModeEnum;
import com.abbyy.FREngine.Ref;
import com.abbyy.FREngine.ResolutionCorrectionModeEnum;
import com.abbyy.FREngine.TXTExportFormatEnum;
import com.abbyy.FREngine.TextEncodingTypeEnum;
import com.abbyy.FREngine.ThreeStatePropertyValueEnum;

@Service
public class FineReaderService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FineReaderService.class);

	@Autowired
	private Configuration configuration;

	private IEngine engine;

	public void processaImagem(String caminho, String fileName) throws Exception {
		Boolean processaOCR = false;
		try {
			LOGGER.info("Iniciando OCR do arquivo: " + fileName);
			this.carregarEngine();
			this.configurarFREngine(caminho);
			FileImageSource imageSource = new FileImageSource(fileName);
			if (imageSource.IsEmpty()) {
				LOGGER.info("imageSource vazio");
			} else {
				IBatchProcessor batchProcessor = engine.CreateBatchProcessor();
				IPrepareImageMode prepareImageMode = createPrepareImageMode();
				IPageProcessingParams pageProcessing = createPageProcessingParams();
				batchProcessor.Start(imageSource, null, prepareImageMode, pageProcessing, null);
				IFRPage page = batchProcessor.GetNextProcessedPage();
				Gson gson = new Gson();
				Map<String, List<CharacterData>> characterDataMap = new HashMap<>();
				while (page != null) {
					String path = page.getSourceImagePath();
					String extension = getFileExtension(path);
					String resultFilePath = path.substring(0, path.length() - extension.length() - 1) + ".txt";
					ITextExportParams textExportParams = engine.CreateTextExportParams();
					textExportParams.setAppendToEnd(true);
					textExportParams.setEncodingType(TextEncodingTypeEnum.TET_UTF8);
					textExportParams.setExportFormat(TXTExportFormatEnum.TEF_TXT);

					page.Export(resultFilePath, FileExportFormatEnum.FEF_TextUnicodeDefaults, textExportParams);

					List<CharacterData> characterDataList = characterDataMap.get(resultFilePath);
					if (characterDataList == null) {
						characterDataList = new ArrayList<>();
						characterDataMap.put(resultFilePath, characterDataList);
					}
					characterDataList.add(this.getCharacterData(page));
					page = batchProcessor.GetNextProcessedPage();
				}

				for (Map.Entry<String, List<CharacterData>> entry : characterDataMap.entrySet()) {

					LOGGER.info("Salvando documento pós-processado: " + entry.getKey());

					Path filePath = Paths.get(entry.getKey());

					String json = gson.toJson(entry.getValue());
					String texto = new String(Files.readAllBytes(filePath), StandardCharsets.UTF_8);

					try {
						String filename = entry.getKey().substring(0, entry.getKey().length() - 4);
						LOGGER.info("Salvando arquivo original: " + entry.getKey());
						Files.write(new File(filename + ".original.txt").toPath(), texto.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
					} catch (IOException e) {
						LOGGER.info("Erro ao escrever arquivo" + e);
						e.printStackTrace();
					}

					LOGGER.debug("Pasta: {}", filePath.getParent().getFileName().toString());

					texto = TextCleanerService.posProcessamento(texto);

					if (texto == null || texto.isEmpty()) {
						texto = "Sem conteúdo.";
					} else {
						Files.write(Paths.get(entry.getKey() + ".dados"), json.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE,
								StandardOpenOption.TRUNCATE_EXISTING);
					}

					Files.write(filePath, texto.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

				}
			}
			LOGGER.info("Salvando registros de acesso...");
		} catch(Exception e){
			LOGGER.error(e.getMessage());
		} finally {
			this.unloadEngine();
		}
	}

	private void carregarEngine() throws Exception {
		try {
			engine = Engine.GetEngineObject(configuration.getCaminhoLibraryFr(), configuration.getSerialFr());
			ILicenses licenses = engine.GetAvailableLicenses(configuration.getSerialFr(),"");
			engine.SetCurrentLicense(licenses.getElement(0) , true);
		} catch (java.lang.UnsatisfiedLinkError e) {
			LOGGER.error("Aconteceu um erro ao carregar engine do FineReader");
			LOGGER.error(e.getMessage());
			throw new Exception("Aconteceu um erro ao carregar engine do FineReader", e);
		}
	}

	private void configurarFREngine(String filePath) throws Exception {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd-HHmmss");
			String dateString = formatter.format(new Date());
			String engineLog = configuration.getCaminhoLogFr().concat(File.separator).concat(filePath).concat(File.separator)
					.concat(dateString).concat(UUID.randomUUID().toString()).concat(".log");

			File elFile = new File(engineLog);
			Files.createDirectories(elFile.getParentFile().toPath());

			engine.setMessagesLanguage(MessagesLanguageEnum.ML_PortugueseBrazilian);
			engine.LoadPredefinedProfile(configuration.getProfile());
			engine.StartLogging(engineLog, true);

		} catch (Exception e) {
			throw new Exception("Aconteceu um erro ao carregar engine do FineReader", e);
		}
	}

	public IPrepareImageMode createPrepareImageMode() {
		IPrepareImageMode prepareImageMode = engine.CreatePrepareImageMode();
		prepareImageMode.setEnhanceLocalContrast(true);
		prepareImageMode.setCorrectSkew(true);
		return prepareImageMode;
	}

	public IPageProcessingParams createPageProcessingParams() {
		IPagePreprocessingParams pagePreprocessingParams = createPagePreprocessingParams();
		IObjectsExtractionParams objectsExtractionParams = createObjectExtractionParams();
		IRecognizerParams recognizerParams = createRecognizerParams();
		IPageAnalysisParams pageAnalysisParams = engine.CreatePageAnalysisParams();
		pageAnalysisParams.setDetectText(true);
		pageAnalysisParams.setEnableTextExtractionMode(true);
		recognizerParams.setBalancedMode(false);
		IPageProcessingParams pageProcessing = engine.CreatePageProcessingParams();
		pageProcessing.setObjectsExtractionParams(objectsExtractionParams);
		pageProcessing.setPagePreprocessingParams(pagePreprocessingParams);
		pageProcessing.setRecognizerParams(recognizerParams);
		pageProcessing.setPageAnalysisParams(pageAnalysisParams);
		pageProcessing.setPerformPreprocessing(true);
		pageProcessing.setPerformRecognition(true);
		pageProcessing.setPerformAnalysis(true);

		return pageProcessing;

	}

	public IRecognizerParams createRecognizerParams() {

		IRecognizerParams recognizerParams = engine.CreateRecognizerParams();

		recognizerParams.SetPredefinedTextLanguage("PortugueseBrazilian");
		recognizerParams.setLowResolutionMode(false);

		return recognizerParams;

	}

	public IObjectsExtractionParams createObjectExtractionParams() {

		IObjectsExtractionParams objectsExtractionParams = engine.CreateObjectsExtractionParams();

		objectsExtractionParams.setDetectTextOnPictures(true);
		objectsExtractionParams.setEnableAggressiveTextExtraction(true);
		objectsExtractionParams.setRemoveTexture(true);
		objectsExtractionParams.setRemoveGarbage(true);

		return objectsExtractionParams;

	}

	public IPagePreprocessingParams createPagePreprocessingParams() {

		IPagePreprocessingParams pagePreprocessingParams = engine.CreatePagePreprocessingParams();
		IOrientationDetectionParams orientationDetectionParams = engine.CreateOrientationDetectionParams();

		pagePreprocessingParams.setCorrectInvertedImage(true);
		pagePreprocessingParams.setCorrectOrientation(true);
		pagePreprocessingParams.setCorrectShadowsAndHighlights(ThreeStatePropertyValueEnum.TSPV_Auto);
		pagePreprocessingParams.setResolutionCorrectionMode(ResolutionCorrectionModeEnum.RCM_Auto);

		orientationDetectionParams.setOrientationDetectionMode(OrientationDetectionModeEnum.ODM_Thorough);
		pagePreprocessingParams.setOrientationDetectionParams(orientationDetectionParams);

		return pagePreprocessingParams;

	}

	private CharacterData getCharacterData(IFRPage document) {
		Ref<int[]> pageNumbers = new Ref<int[]>();
		Ref<int[]> leftBorders = new Ref<int[]>();
		Ref<int[]> topBorders = new Ref<int[]>();
		Ref<int[]> rightBorders = new Ref<int[]>();
		Ref<int[]> bottomBorders = new Ref<int[]>();
		Ref<int[]> confidences = new Ref<int[]>();
		Ref<boolean[]> isSuspicious = new Ref<boolean[]>();

		CharacterData cd = new CharacterData();

		if (document.getPlainText().getSymbolsCount() >= 1) {

			document.getPlainText().GetCharacterData(pageNumbers, leftBorders, topBorders, rightBorders, bottomBorders, confidences, isSuspicious);

			cd.setPageNumbers(pageNumbers.get());
			cd.setLeftBorders(leftBorders.get());
			cd.setTopBorders(topBorders.get());
			cd.setRightBorders(rightBorders.get());
			cd.setBottomBorders(bottomBorders.get());
			cd.setConfidences(confidences.get());
			cd.setSuspicious(isSuspicious.get());
		} else {
			int intZerado[] = { 0 };
			boolean bolZerado[] = { false };
			cd.setPageNumbers(intZerado);
			cd.setLeftBorders(intZerado);
			cd.setTopBorders(intZerado);
			cd.setRightBorders(intZerado);
			cd.setBottomBorders(intZerado);
			cd.setConfidences(intZerado);
			cd.setSuspicious(bolZerado);
		}

		return cd;
	}

	private void unloadEngine() throws Exception {

		try {

			LOGGER.info("Deinitializing Engine...");

			engine = null;

			Engine.Unload();
			Engine.DeinitializeEngine();

			LOGGER.info("Deinitializing Engine OK");

		} catch (Exception e) {
			throw new Exception("Aconteceu um erro ao finalizar engine do FineReader: {}", e);
		}

	}
	
	public static String getFileExtension(String fileName) {
		String extension = "";

		int pointIndex = fileName.lastIndexOf('.');
		int slashIndex = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));

		if (pointIndex > slashIndex) {
			extension = fileName.substring(pointIndex + 1);
		}

		return extension;
	}

}
