package com.mp.ba.pocmpocr.service;

import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

public class TextCleanerService {

    private static final String E_OUTRO = "e outro";

    private static final String E_OUTROS = "e outros";

    private static final Logger LOGGER = LoggerFactory.getLogger(TextCleanerService.class);

    private static final String INICIO_CABECALHO = "PODER JUDICIÁRIO DO ESTADO DA BAHIA";
    private static final String FIM_CABECALHO = "@tjba.jus.br";
    private static final int LIMITE_CABECALHO = 10;
    private static final Pattern EMAIL_PATTERN = Pattern
            .compile("(?:(?:\\r\\n)?[ \\t])*(?:(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*)|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*:(?:(?:\\r\\n)?[ \\t])*(?:(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*)(?:,\\s*(?:(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*|(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)*\\<(?:(?:\\r\\n)?[ \\t])*(?:@(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*(?:,@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*)*:(?:(?:\\r\\n)?[ \\t])*)?(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\"(?:[^\\\"\\r\\\\]|\\\\.|(?:(?:\\r\\n)?[ \\t]))*\"(?:(?:\\r\\n)?[ \\t])*))*@(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*)(?:\\.(?:(?:\\r\\n)?[ \\t])*(?:[^()<>@,;:\\\\\".\\[\\] \\000-\\031]+(?:(?:(?:\\r\\n)?[ \\t])+|\\Z|(?=[\\[\"()<>@,;:\\\\\".\\[\\]]))|\\[([^\\[\\]\\r\\\\]|\\\\.)*\\](?:(?:\\r\\n)?[ \\t])*))*\\>(?:(?:\\r\\n)?[ \\t])*))*)?;\\s*)");

    public static String removerLinhasEmBranco(String texto) {

        if (texto == null) {
            return null;
        }

        return texto.replaceAll("(?m)^[ \t]*\r?\n", "");
    }

    public static String removerQuebrasDeLinha(String texto) {

        if (texto == null) {
            return null;
        }

        StringBuilder textoFinal = new StringBuilder();

        for (String linha : texto.split("\r\n|\n")) {

            if (!StringUtils.isEmpty(linha.trim())) {
                textoFinal.append(linha);

                if (linha.endsWith(".")) {
                    textoFinal.append('\n');
                } else {
                    textoFinal.append(' ');
                }

            }

        }

        return textoFinal.toString();

    }

    public static String removerCabecalho(String texto) {

        if (texto == null) {
            return null;
        }

        boolean limpar = false;
        int contadorLinha = 0;

        StringBuilder textoRemover = new StringBuilder();
        StringBuilder textoFinal = new StringBuilder();

        for (String linha : texto.split("\n")) {
            String[] palavrasLinha = linha.split(" ");
            if (linha.endsWith(FIM_CABECALHO) || isEmail(palavrasLinha[palavrasLinha.length - 1]) || (linha.contains("E-mail") && contadorLinha != 0)) {
                limpar = false;
                textoRemover = new StringBuilder();
                if (contadorLinha <= LIMITE_CABECALHO) {
                    continue;
                }
            }

            if (linha.trim().startsWith(INICIO_CABECALHO)) {
                limpar = true;
            }

            if (limpar) {
                contadorLinha++;
                textoRemover.append(linha).append("\n");
            } else {
                contadorLinha = 0;
                textoFinal.append(linha).append("\n");
            }

            if (contadorLinha == LIMITE_CABECALHO) {
                if (limpar) {
                    textoFinal.append(textoRemover);
                }
                limpar = false;
            }

        }
        if (limpar) {
            textoFinal.append(textoRemover);
        }

        
        texto = textoFinal.toString();
        
        if (texto.isEmpty() || texto.length() < 1) {
            return null;
        } else {
            return texto.substring(0, texto.length() - 1);
        }
    }

    private static boolean isEmail(String email) {
        return (EMAIL_PATTERN.matcher(email).matches());
    }

    public static String removerLixo(String texto) {

        if (texto == null) {
            return null;
        }

        texto = texto.replaceAll("\\&", " \\& ");
        texto = texto.replaceAll("\\^", " ");
        texto = texto.replaceAll("[\\\\»\\•\\%\\°\\~\\#\\♦\\®]", " ");
        texto = texto.replaceAll("fl\\.", "folha ");
        texto = texto.replaceAll("fls\\.", "folhas ");
        texto = texto.replaceAll("Fl\\.", "folha ");
        texto = texto.replaceAll("Fls\\.", "folhas ");
        texto = texto.replaceAll("FL\\.", "folha ");
        texto = texto.replaceAll("FLS\\.", "folhas ");
        texto = texto.replaceAll("art\\.", "artigo ");
        texto = texto.replaceAll("arts\\.", "artigos ");
        texto = texto.replaceAll("ART\\.", "artigo ");
        texto = texto.replaceAll("ARTS\\.", "artigos ");
        texto = texto.replaceAll("Art\\.", "artigo ");
        texto = texto.replaceAll("Arts\\.", "artigos ");
        texto = texto.replaceAll("\\§", "paragrafo ");
        texto = texto.replaceAll("\\■", " ");
        texto = texto.replaceAll("\\|", " ");
        // texto = texto.replaceAll("(\\d\\s)*\\d", "");
        texto = texto.replaceAll("(\\d\\s)*", "");
        texto = texto.replaceAll("[\\£\\<\\>\\®]", " ");
        texto = texto.replaceAll("\t", "");
        texto = texto.replaceAll("\\s[0-9]$", "");
        texto = texto.replaceAll("\\s[bdfghjlmnpqrstuwxyzBCDFGHJLMNPQRSTUWXYZ]\\s", " ");
        texto = texto.replaceAll("\\s-\\s", "-");
        texto = texto.replaceAll("£$", "");
        texto = texto.replaceAll("\\^$", "");
        texto = texto.replaceAll("jjj", " ");
        texto = texto.replaceAll("<3$", "");
        texto = texto.replaceAll("<$", "");
        texto = texto.replaceAll("\\s[bcdfghijlmnpqrstuvwxyzBCDFGHIJLMNPQRSTUVWXYZ]$", " ");
        texto = texto.replaceAll("\\«", "");
        texto = texto.replaceAll("\\*", "");
        texto = texto.replaceAll("\\“", "");
        texto = texto.replaceAll("\\”", "");
        texto = texto.replaceAll("\\\"", "");
        texto = texto.replaceAll("\\(ões\\)", "");
        texto = texto.replaceAll("\\(ão\\)", "");
        texto = texto.replaceAll("\\(as\\)", "");
        texto = texto.replaceAll("\\(a\\)", "");
        texto = texto.replaceAll("\\(s\\)", "");
        texto = texto.replaceAll("\\(o\\)", "");
        texto = texto.replaceAll("[ ]{2,}", " ");

        if (texto.length() <= 4) {
            return null;
        }

        return texto.trim();

    }
    
    public static String posProcessamentoEdital(String texto) {
        
        if (texto == null) {
            return null;
        }
        
        StringBuilder textoFinal = new StringBuilder();
        for (String linha : texto.replaceAll("\r", "").split("\n")) {
            if (linha.toLowerCase().startsWith("réu") || linha.toLowerCase().startsWith("reu") || linha.toLowerCase().endsWith(E_OUTROS) || linha.toLowerCase().endsWith(E_OUTRO)) {
                LOGGER.debug("Removendo linha por ser do tipo Edital: {}", linha);
            } else {
                textoFinal.append(linha);
                textoFinal.append('\n');
            }
        }

        texto = textoFinal.toString();
        
        if (texto.isEmpty() || texto.length() < 1) {
            return null;
        } else {
            return texto.substring(0, texto.length() - 1);
        }
    }

    public static String posProcessamento(String texto) {

        if (texto == null) {
            return null;
        }

        texto = TextCleanerService.removerBOM(texto);
        texto = TextCleanerService.removerJusticaGratuita(texto);
        texto = TextCleanerService.removerLixo(texto);
        texto = TextCleanerService.removerLinhasEmBranco(texto);
        texto = TextCleanerService.removerCabecalho(texto);
        texto = TextCleanerService.removerQuebrasDeLinha(texto);
        texto = TextCleanerService.removerStringsInvalidas(texto);
        texto = TextCleanerService.removerEspacoDuplicado(texto);
        texto = TextCleanerService.corrigirPontoVirgula(texto);
        
        return texto;

    }

    private static String corrigirPontoVirgula(String texto) {
    	
        if (texto == null) {
            return null;
        }

        texto = texto.replaceAll("ponha.", "ponha,");

        return texto;
        
	}

	private static String removerEspacoDuplicado(String texto) {

        if (texto == null) {
            return null;
        }

        texto = texto.replaceAll("\\s+", " ");

        return texto;
    }

    private static String removerBOM(String texto) {

        texto = texto.replaceAll("\\r", "");
        texto = texto.replace("\uFEFF", "");

        return texto;

    }

    private static String removerStringsInvalidas(String texto) {
        if (texto == null) {
            return null;
        }
        StringBuilder textoFinal = new StringBuilder();
        for (String linha : texto.split("\n")) {
            int totalChar = linha.length();
            String textoSemEspaco = linha.replace(" ", "");
            int charSemEspaco = textoSemEspaco.length();
            if (!(totalChar * 0.3 < totalChar - charSemEspaco)) {
                textoFinal.append(linha);
                textoFinal.append('\n');
            }
        }
        texto = textoFinal.toString();
        if (texto.isEmpty() || texto.length() < 1) {
            return null;
        } else {
            return texto.substring(0, texto.length() - 1);
        }
    }

    public static String removerJusticaGratuita(String texto) {

        if (texto == null) {
            return null;
        }

        return texto.replaceAll("Justiça Gratuita", "");
    }

}
