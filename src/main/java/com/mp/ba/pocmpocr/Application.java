package com.mp.ba.pocmpocr;

import com.mp.ba.pocmpocr.config.Configuration;
import com.mp.ba.pocmpocr.service.InqueritoFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner{

    @Autowired
    Configuration configuration;

    @Autowired
    InqueritoFileReader inqueritoFileReader;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args){
        inqueritoFileReader.readAllFiles(configuration.getCaminhoArquivos());
        System.exit(0);
    }
}
