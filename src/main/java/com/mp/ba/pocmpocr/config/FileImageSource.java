package com.mp.ba.pocmpocr.config;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.abbyy.FREngine.IFileAdapter;
import com.abbyy.FREngine.IImageSource;

public class FileImageSource implements IImageSource {

	private Iterator<String> iterator;
	private boolean empty;

	public FileImageSource(String fileName) {
		File file1 = new File(fileName);
		List<String> pdfList = new ArrayList<>();
		pdfList.add(file1.getAbsolutePath());
		iterator = pdfList.iterator();
		empty = !iterator.hasNext();
	}

	public IFileAdapter GetNextImageFile() {
		if (empty) {
			return null;
		}
		PdfFileAdapter fileAdapter = new PdfFileAdapter(iterator.next());
		empty = !iterator.hasNext();
		return fileAdapter;
	}

	public boolean IsEmpty() {
		return empty;
	}

}
