package com.mp.ba.pocmpocr.config;

import com.abbyy.FREngine.IFileAdapter;
import com.abbyy.FREngine.IIntsCollection;

public class PdfFileAdapter implements IFileAdapter {

	private String fileName;

	public PdfFileAdapter(String fileName) {
		this.fileName = fileName;
	}

	public String GetFileName() {
		return fileName;
	}

	public IIntsCollection GetPagesToProcess() {
		return null;
	}

	public String GetPassword() {
		return "";
	}

}
