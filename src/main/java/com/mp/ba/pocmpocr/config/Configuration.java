package com.mp.ba.pocmpocr.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Configuration {

	@Value("${caminho.arquivos}")
	private String caminhoArquivos;

	@Value("${finereader.library.caminho}")
	private String caminhoLibraryFr;

	@Value("${finereader.serialnumber}")
	private String serialFr;

	@Value("${finereader.profile}")
	private String profile;

	@Value("${finereader.logpath}")
	private String caminhoLogFr;

	public String getCaminhoArquivos() {
		return caminhoArquivos;
	}

	public String getCaminhoLibraryFr() {
		return caminhoLibraryFr;
	}

	public String getSerialFr() {
		return serialFr;
	}

	public String getProfile() {
		return profile;
	}

	public String getCaminhoLogFr() {
		return caminhoLogFr;
	}


}
