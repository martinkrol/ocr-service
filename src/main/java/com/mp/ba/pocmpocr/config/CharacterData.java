package com.mp.ba.pocmpocr.config;

public class CharacterData {

    private int[] pageNumbers;
    private int[] leftBorders;
    private int[] topBorders;
    private int[] rightBorders;
    private int[] bottomBorders;
    private int[] confidences;
    private boolean[] suspicious;
    
	public int[] getPageNumbers() {
		return pageNumbers;
	}
	public void setPageNumbers(int[] pageNumbers) {
		this.pageNumbers = pageNumbers;
	}
	public int[] getLeftBorders() {
		return leftBorders;
	}
	public void setLeftBorders(int[] leftBorders) {
		this.leftBorders = leftBorders;
	}
	public int[] getTopBorders() {
		return topBorders;
	}
	public void setTopBorders(int[] topBorders) {
		this.topBorders = topBorders;
	}
	public int[] getRightBorders() {
		return rightBorders;
	}
	public void setRightBorders(int[] rightBorders) {
		this.rightBorders = rightBorders;
	}
	public int[] getBottomBorders() {
		return bottomBorders;
	}
	public void setBottomBorders(int[] bottomBorders) {
		this.bottomBorders = bottomBorders;
	}
	public int[] getConfidences() {
		return confidences;
	}
	public void setConfidences(int[] confidences) {
		this.confidences = confidences;
	}
	public boolean[] getSuspicious() {
		return suspicious;
	}
	public void setSuspicious(boolean[] suspicious) {
		this.suspicious = suspicious;
	}
    
    
    
}
